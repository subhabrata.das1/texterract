import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:google_mlkit_text_recognition/google_mlkit_text_recognition.dart';

class TextView extends StatefulWidget {
  final RecognizedText recognizedText;
  const TextView({required this.recognizedText, super.key});

  @override
  State<TextView> createState() => _TextViewState();
}

class _TextViewState extends State<TextView> {
  String? selectedtext;
  String? translatedText;

  Future translate() async {
    String apiKey = 'AIzaSyCqUQPhrjLv-pRKhBrls64v5h0gPDG-n5U';
    String lang = 'hi-IN';
    Uri url = Uri.parse(
        'https://translation.googleapis.com/language/translate/v2?target=$lang&key=$apiKey&q=$selectedtext');

    final response = await http.post(url);

    if (response.statusCode == 200) {
      final body = json.decode(response.body);
      List translations = body['data']['translations'];
      String translation = translations.first['translatedText'];
      setState(() {
        translatedText = translation;
        debugPrint(translatedText);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Extracted Text"),
      ),
      body: SingleChildScrollView(
          child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: SelectableText(
                onSelectionChanged: (selection, cause) {
                  setState(() {
                    selectedtext = widget.recognizedText.text
                        .substring(selection.start, selection.end);
                  });
                },
                widget.recognizedText.text,
                style: const TextStyle(fontSize: 30),
                contextMenuBuilder: (BuildContext context,
                    EditableTextState editableTextState) {
                  List<ContextMenuButtonItem> buttonItem =
                      editableTextState.contextMenuButtonItems;
                  buttonItem.insert(
                      buttonItem.length,
                      ContextMenuButtonItem(
                          label: "Translate",
                          onPressed: () {
                            translate();
                            FocusScope.of(context).unfocus();
                          }));
                  return AdaptiveTextSelectionToolbar.buttonItems(
                      buttonItems: buttonItem,
                      anchors: editableTextState.contextMenuAnchors);
                },
                enableInteractiveSelection: true,
              ),
            ),
            if (translatedText != null)
              Column(
                children: [
                  const Text("Translated Text",
                      style: const TextStyle(
                          fontSize: 22, fontWeight: FontWeight.bold)),
                  SelectableText(
                    translatedText.toString(),
                    style: const TextStyle(fontSize: 30),
                  )
                ],
              ),
          ],
        ),
      )),
    );
  }
}
