// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:google_mlkit_text_recognition/google_mlkit_text_recognition.dart';
import 'package:image_picker/image_picker.dart';
import 'package:texterract/pages/textview.dart';

class ImageAccess extends StatefulWidget {
  const ImageAccess({super.key});

  @override
  State<ImageAccess> createState() => _ImageAccessState();
}

class _ImageAccessState extends State<ImageAccess> {
  Future setImage({required ImageSource source}) async {
    XFile? file;

    file = await ImagePicker().pickImage(
        source: source, maxHeight: 1000, maxWidth: 1000, imageQuality: 100);
    if (file != null) {
      String xfile = file.path;

      final textRecognizer = TextRecognizer();
      final inputImage = InputImage.fromFilePath(xfile);
      final recognizedText = await textRecognizer.processImage(inputImage);

      setState(() {
        RecognizedText recogText = recognizedText;
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => TextView(
              recognizedText: recogText,
            ),
          ),
        );
      });
    } else {
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton.icon(
                  onPressed: () {
                    showModalBottomSheet(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(20),
                          ),
                        ),
                        context: context,
                        builder: (BuildContext context) {
                          return Container(
                            height: 200,
                            decoration: BoxDecoration(
                                color: Color.fromARGB(255, 255, 255, 255),
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20),
                                  topRight: Radius.circular(20),
                                ),
                                // ignore: prefer_const_literals_to_create_immutables
                                boxShadow: [
                                  BoxShadow(
                                    color: Color.fromARGB(255, 54, 127, 244),
                                    blurRadius: 5, // soften the shadow
                                    spreadRadius: 0, //extend the shadow
                                    offset: Offset(
                                      0, // Move to right 10  horizontally
                                      -3.0, // Move to
                                    ),
                                  )
                                ]),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: const [
                                    Padding(
                                      padding: EdgeInsets.all(15),
                                      child: Text(
                                        "Uploade Profile Image",
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'Ubuntu',
                                          fontSize: 17,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 25,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Column(
                                      children: [
                                        GestureDetector(
                                          onTap: () {
                                            setImage(
                                                source: ImageSource.camera);
                                          },
                                          child: CircleAvatar(
                                            radius: 30,
                                            backgroundColor: Color.fromARGB(
                                                238, 120, 184, 233),
                                            child: Icon(
                                              size: 21,
                                              Icons.camera_alt,
                                              color:
                                                  Color.fromARGB(255, 1, 5, 16),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "Camera",
                                          style: TextStyle(
                                            color: Color.fromARGB(
                                                163, 0, 112, 209),
                                            fontWeight: FontWeight.w600,
                                            fontFamily: 'Ubuntu',
                                          ),
                                        )
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        GestureDetector(
                                          onTap: (() => setImage(
                                              source: ImageSource.gallery)),
                                          child: CircleAvatar(
                                            radius: 30,
                                            backgroundColor: Color.fromARGB(
                                                238, 120, 184, 233),
                                            child: Icon(
                                              size: 21,
                                              Icons.image,
                                              color:
                                                  Color.fromARGB(255, 1, 5, 16),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "Gallery",
                                          style: TextStyle(
                                            color: Color.fromARGB(
                                                163, 0, 112, 209),
                                            fontWeight: FontWeight.w600,
                                            fontFamily: 'Ubuntu',
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                )
                              ],
                            ),
                          );
                        });
                  },
                  icon: Icon(Icons.camera),
                  label: Text("AccessImage"))
            ]),
      ),
    );
  }
}
